import os
import shutil
from filecmp import dircmp
from distutils.dir_util import copy_tree

#This script compares two duplicate directories, A and B, and updates A with any newer content found in B.

#Place this script and both directories in the same folder at the same level, and enter the correct paths and folder names below.

dcmp = dircmp('FolderA', 'FolderB') #Enter folder names

latest_files = []
latest_dirs = []

def diff_files(dcmp):  #Finds modified files
    for name in dcmp.diff_files:
        if name.endswith(".DS_Store") == False:
            files = [os.path.abspath(os.path.join(dcmp.left, '') + name), os.path.abspath(os.path.join(dcmp.right, '') + name)]
            files.sort(key=os.path.getmtime)  #sort according to timestamp
            if files[-1] == os.path.abspath(os.path.join(dcmp.left, '') + name):     #most recent file is in A, keep and continue
                continue
            else:
                latest_files.append(os.path.abspath(files[-1])) #most recent file in B; add to list of files to be copied from B to A

    for sub_dcmp in dcmp.subdirs.values():
        diff_files(sub_dcmp)

def missing(dcmp): #Finds files/directories that are in B only
    for name in dcmp.right_only:
        if os.path.isdir(os.path.abspath(os.path.join(dcmp.right, '') + name)):
            latest_dirs.append(os.path.abspath(os.path.join(dcmp.right, '') + name))
        else:
            if name.endswith(".DS_Store") == False:
                latest_files.append(os.path.abspath(os.path.join(dcmp.right, '') + name))

missing(dcmp)
diff_files(dcmp)

for path in latest_dirs:
    rel = os.path.relpath(path, '/Path/To/FolderB')
    new_path = os.path.join('/Path/To/FolderA', rel)
    copy_tree(path, new_path)

for path in latest_files:
    rel = os.path.relpath(path, '/Path/To/FolderB')
    new_path = os.path.join('/Path/To/FolderA', rel)
    shutil.copy2(path, new_path)
